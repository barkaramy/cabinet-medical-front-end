import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import VisitCard from "../components/VisitCard/VisitCard"
import homeImage from '../assets/images/home-image.jpg'
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from "@material-ui/core"
import 'antd/dist/antd.css';
import Appointment from '../components/Appointment/Appointment';
import ServicesProvided from "../components/ServicesProvided/ServicesProvided"
import Emergency from "../components/Emergency/Emergency"
import Scroll from "../components/Scroll/Scroll"


const IndexPage = () => {
  return (
    <Layout>
      <Scroll showBelow={250} />
      <SEO title="Home" />
      <Grid alignContent="center" justify="center" alignItems="center" container direction="column" width={1}>
        <VisitCard />
        <Appointment />
        <ServicesProvided />
        <Emergency />
      </Grid>
    </Layout>
  )
}

export default IndexPage
