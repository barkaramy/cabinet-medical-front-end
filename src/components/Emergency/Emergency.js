import React from "react"
import { Grid } from "@material-ui/core"
import { faHSquare, faExclamationCircle } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { makeStyles } from "@material-ui/core/styles"
import homeImage from "../../assets/images/home-image.jpg"

const useStyle = makeStyles(() => ({
  emergencyContainer: {
    fontSize: '16px',
    margin: '0',
    '& h1': {
      textAlign: 'center'
    },
    height: '400px',
    opacity: '0.8',
    color: 'white',

  }
}))

function Emergency() {
  const classes = useStyle()
  return (
    <Grid alignContent="center" justify="center" alignItems="center" container direction="column" xs={12} width={1} style={{paddingTop: 30}}>
      <Grid
        id="Emergency"
        container
        direction="column"
        justify="center"
        alignItems="center" width={1}>
        <FontAwesomeIcon icon={faExclamationCircle} size="xs" style={{ fontSize: '96px', color: '#F6D3BD'}}/>
        <Grid container direction={"column"} justify={'center'} alignItems={'center'} className={classes.emergencyContainer} width={1}>
          <h1>
            En cas d'urgence<br /> Appelez le secrétariat au 01 67 89 78 15<br />
          </h1>
          <h1>
            Votre dermatologue vous recevra<br /> dans les meilleurs délais.<br />
          </h1>
          <h1>
            En dehors des horaires d'ouverture<br /> Composez le 15<br />
          </h1>
        </Grid>
      </Grid>


    </Grid>
  )
}

export default Emergency