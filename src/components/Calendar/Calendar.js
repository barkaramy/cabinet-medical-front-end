import React from "react"
import { Helmet } from "react-helmet"

const Calendar = () => {
    return (
        <>
            <div className="calendly-inline-widget" data-url="https://calendly.com/jerome-celebi/rendez-vous-medical" style={{ height: "700px", width: "100%" }} ></div>
            <Helmet>
                {typeof window != "undefined" && <script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
                }
            </Helmet>
        </>
    )
}

export default Calendar