import React from "react"
import { Button, Card } from "antd"
import { CardActionArea, CardContent, CardMedia, Grid, Typography, CardActions } from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles"
import homeImage from "../../assets/images/home-image.jpg"
import Title from "../Title/Title"
import cardimage1 from "../../assets/images/home-image.jpg"
import cardimage2 from "../../assets/images/Cancer-de-la-peau.jpg"

const useStyle = makeStyles(() => ({
  image: {
    backgroundImage: `url(${homeImage})`,
    backgroundSize: "cover",
    height: "600px"
  },
  root: {
    maxWidth: 345,
    borderRadius: 8
  },
  media: {
    height: 140
  }
}))

function ServicesProvided() {
  const classes = useStyle()
  return (
    <Grid alignContent="center" justify="center" alignItems="center" container direction="column" width={1}>
      <Title>Acte pratiqués</Title>
      <Grid
        id='Pratique'
        container
        direction="row"
        justify="space-around"
        alignItems="center" width={1}>
        <Card className={classes.root}>
          <CardActionArea>
            <CardMedia
              className={classes.media}
              image={cardimage1}
              title="Contemplative Reptile"
            />
            <CardContent>
              <Typography gutterBottom variant="h6" component="h6">
                Le Vieillissement Cutané
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                Le principe des injections de comblement est la restauration d’un volume tissulaire perdu.
                Cet acte peut être réalisé par exemple pour corriger un défaut de naissance ou survenu à la suite d’une maladie ou d’un accident.
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button size="medium" color="primary"
                    style={{ borderRadius: 4, backgroundColor: "#F6D3BD", borderColor: "#F6D3BD", color: "white" }}>
              En savoir plus
            </Button>
          </CardActions>
        </Card>
        <Card className={classes.root}>
          <CardActionArea>
            <CardMedia
              className={classes.media}
              image={cardimage2}
              title="Contemplative Reptile"
            />
            <CardContent>
              <Typography gutterBottom variant="h6" component="h2">
                Cancer cutané
              </Typography>
              <Typography variant="body2" color="textSecondary" component="span">
                Ils sont facilement guérissables dans la majorité des cas. Non traités à temps, ils peuvent cependant
                entraîner des conséquences sérieuses. Ils apparaissent le plus souvent après 50 ans, sur les parties
                découvertes du corps (visage, cou, épaules, avant-bras, mains…)
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button size="medium" color="primary"
                    style={{ borderRadius: 4, backgroundColor: "#F6D3BD", borderColor: "#F6D3BD", color: "white" }}>
              En savoir plus
            </Button>
          </CardActions>
        </Card>
        <Card className={classes.root}>
          <CardActionArea>
            <CardMedia
              className={classes.media}
              image={cardimage1}
              title="Contemplative Reptile"
            />
            <CardContent>
              <Typography gutterBottom variant="h6" component="h6">
                Le Vieillissement Cutané
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                Le principe des injections de comblement est la restauration d’un volume tissulaire perdu.
                Cet acte peut être réalisé par exemple pour corriger un défaut de naissance ou survenu à la suite d’une maladie ou d’un accident.
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button size="medium" color="primary"
                    style={{ borderRadius: 4, backgroundColor: "#F6D3BD", borderColor: "#F6D3BD", color: "white" }}>
              En savoir plus
            </Button>
          </CardActions>
        </Card>
      </Grid>
    </Grid>
  )
}

export default ServicesProvided