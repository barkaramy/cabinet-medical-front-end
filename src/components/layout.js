/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"

import Header from "./header"
import "./layout.css"
import Footer from '../components/Footer/Footer'

const Layout = ({ children }) => {
  const sections = [
    { title: 'Accueil', url: '#' },
    { title: 'Le cabinet', url: '#Pratique' },
    { title: 'Dr. John Doe', url: '#' },
    { title: 'Prendre rendez-vous', url: '#Appointment' },
    { title: 'Urgences', url: '#Emergency' },
  ];

  return (
    <>
      <Header sections={sections} />
      <div
        style={{ paddingBottom: 50 }}
      >
        <main>{children}</main>
      </div>
      <Footer />
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
