import React from "react"
import { makeStyles } from '@material-ui/core/styles';
import homeImage from '../../assets/images/home-image.jpg'
import { Grid } from "@material-ui/core"

const useStyle = makeStyles(() => ({
  image: {
    backgroundImage: `url(${homeImage})`,
    backgroundSize: 'cover',
    height: "600px"
  },
  visitCardContainer: {
    display: 'flex',
    flexDirection: 'column',
    fontSize: '16px',
    alignItems: 'center',
    margin: '0',
    '& h1': {
      textAlign: 'center'
    },
    borderRadius: '50%',
    background: '#F6D3BD',
    height: '400px',
    opacity: '0.8',
    color: 'white',
    width: '400px',
    justifyContent: 'center'

  }
}))

function VisitCard(props) {
  const classes = useStyle();
  return (
    <Grid justify="center" alignItems="center" container className={classes.image}>
      <div className={classes.visitCardContainer}>
        <h1>
          Docteur John Doe<br /> DERMATOLOGUE<br /> 11 rue du pere corentin - 75014 PARIS
        </h1>
        <h2>Tél : 01 67 89 78 15</h2>
      </div>
    </Grid >
  )
}

export default VisitCard