import React from "react"
import { Grid } from "@material-ui/core"
import { Helmet } from 'react-helmet';
import Title from "../Title/Title"
import Calendar from "../Calendar/Calendar"

const Appointment = () => {
    return (
        <Grid alignContent="center" justify="center" alignItems="center" container direction="column" width={1}>
            <Title>Prendre rendez-vous</Title>
            <Grid
                id="Appointment"
                container
                direction="row"
                justify="space-around"
                alignItems="center" width={1}>
                <Calendar />
            </Grid>
        </Grid>
    )
}

export default Appointment