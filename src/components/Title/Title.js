import React from "react"
import { Typography } from "@material-ui/core"
import { makeStyles } from '@material-ui/core/styles';

const useStyle = makeStyles(() => ({
    Title: {
        paddingTop: "2rem",
        paddingBottom: "2rem"
    }
}))

const Title = ({ children }) => {
    const classes = useStyle();
    return (
        <Typography className={classes.Title} variant="h4">{children}</Typography>
    )
}

export default Title